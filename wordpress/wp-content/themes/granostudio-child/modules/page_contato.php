<?php get_header(); ?>

<div class="page-contato">

  <div class="div6">
    <div class="container">

      <div class="row" style="margin-bottom: 50px;">
        <div class="col-sm-6 col-sm-offset-3">
          <h1>Contato</h1>
          <hr class="titulo">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 div-left">
          <div class="row padding">
            <h4>Entre em contato com a Sistran Brasil e descubra como a tecnologia da informação pode ajudar Seguradoras a extrairem valor de oportunidades e a superar desafios. Tudo isso com ajuda de um time:</h4>
          </div>
          <div class="row">
            <div class="col-left-img">
              <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice10@2x.png">
            </div>
            <div class="col-right"> 
              <p>altamente expecializado e que contabiliza 30 anos de experiência em Seguros;</p>
            </div>
          </div>
          <div class="row">
            <div class="col-left-img">
              <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice12@2x.png">
            </div>
            <div class="col-right">
              <p>formado por mais de 160 profissionais no Brasil e 600 na América Latina;</p>
            </div>
          </div>
          <div class="row">
            <div class="col-left-img">
              <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice11@2x.png">
            </div>
            <div class="col-right">
              <p>responsável por processar um terço de todos os prêmios de Seguro de Vida no Brasil.</p>
            </div>
          </div>
          <div class="row padding">
            <h4>Matriz - Sistran São Paulo:</h4>
            <p>Rua Luigi Galvani, 70 - 5º e 6º andares - Brooklin Novo<br>CEP: 04575-020</p>
          </div>
          <div class="row padding">
            <h4>Telefone:</h4>
            <p>+55 11 2192-4400</p>
          </div>
          <a href="http://sistran.tempsite.ws/index.php/sobre/"><button type="button" class="botao botao-home" name="button">Sobre a Sistran</button></a>
        </div>

        <div class="col-sm-6 contato">
          <?php echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato 1"]'); ?>
        </div>
      </div>

    </div>
  </div>

</div>

<?php get_footer(); ?>

<?php get_header(); ?>

<div class="fale-conosco">
  <div class="container">

    <h1>Venha fazer parte da Sistran Brasil!</h1>
    <p class="col-sm-8 col-sm-offset-2">Junte-se a um time de profissionais altamente capacitados em Seguros. Preencha o formulário:</p>

    <div class="col-sm-6 col-sm-offset-3">
      <?php echo do_shortcode('[contact-form-7 id="50" title="Fale Conosco"]'); ?>
    </div>

    <div class="col-sm-12">
      <button type="button" class="botao botao-home" name="button">Sobre a Sistran</button>
    </div>

  </div>

</div>

<?php get_footer(); ?>

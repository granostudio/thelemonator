<?php get_header(); ?>

<div class="img-enviar">
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/enviar.png">	
</div>

<div class="div1">
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/fundo-header.png" class="img-fundo-header">
	
	<h1>NÓS SABEMOS COMO TIRAR<br> O MELHOR DOS LIMÕES...</h1>	
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/limao-rodela2.png" class="img-limao-rodela">
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/seta-baixo.png" class="img-seta">

</div>

<div class="div2" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/img/fundo.jpg');">  
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/limoes2.png" class="img-limoes">

	<h1>CRIANDO UM SABOR</h1> 
	<p class="pos-titulo">refrescante</p>

	<div class="container">
		<div class="row">
			<div class="col-sm-4 padding-left">
				<p>O sabor icomparável do limão e o prazer que só uma limonada bem gelada é capaz de dar estão no copo do Lemonator.</p>
				<p>Para Jack'O, a vida deu limões ele soube fazer a melhor limonada que o mundo já viu!</p>
			</div>
			<div class="col-sm-4" style="position: relative;">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/limonada-copo2.png" class="img-copo">
			</div>
			<div class="col-sm-4 padding-right">
				<p>E agora é sua vez de provar!</p>
				<p>Desfrute dos melhores momentos com um copo de Lemonator bem gelado!</p>
			</div>
		</div>
	</div>
</div>

<script>
	!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?t(require("jquery")):"function"==typeof define&&define.amd?define(["jquery"],t):t(e.$)}(this,function(e){"use strict";function t(e){return"%"==e[e.length-1]}function r(e){var t=e.split(" ");if(1!==t.length)return t.map(function(t){switch(e){case"center":return"50%";case"top":case"left":return"0";case"right":case"bottom":return"100%";default:return t}});switch(e){case"center":return["50%","50%"];case"top":return["50%","0"];case"bottom":return["50%","100%"];case"left":return["0","50%"];case"right":return["100%","50%"];default:return[e,"50%"]}}function i(e,t,r){function i(e,t){var r=s.createShader(e);if(s.shaderSource(r,t),s.compileShader(r),!s.getShaderParameter(r,s.COMPILE_STATUS))throw new Error("compile error: "+s.getShaderInfoLog(r));return r}var o={};if(o.id=s.createProgram(),s.attachShader(o.id,i(s.VERTEX_SHADER,e)),s.attachShader(o.id,i(s.FRAGMENT_SHADER,t)),s.linkProgram(o.id),!s.getProgramParameter(o.id,s.LINK_STATUS))throw new Error("link error: "+s.getProgramInfoLog(o.id));o.uniforms={},o.locations={},s.useProgram(o.id),s.enableVertexAttribArray(0);for(var n,a,u=/uniform (\w+) (\w+)/g,h=e+t;null!=(n=u.exec(h));)a=n[2],o.locations[a]=s.getUniformLocation(o.id,a);return o}function o(e,t){s.activeTexture(s.TEXTURE0+(t||0)),s.bindTexture(s.TEXTURE_2D,e)}function n(e){var t=/url\(["']?([^"']*)["']?\)/.exec(e);return null==t?null:t[1]}function a(e){return e.match(/^data:/)}var s,u=(e=e&&"default"in e?e.default:e)(window),h=function(){function e(e,t,i){var o="OES_texture_"+e,n=o+"_linear",a=n in r,s=[o];return a&&s.push(n),{type:t,arrayType:i,linearSupport:a,extensions:s}}var t=document.createElement("canvas");if(!(s=t.getContext("webgl")||t.getContext("experimental-webgl")))return null;var r={};if(["OES_texture_float","OES_texture_half_float","OES_texture_float_linear","OES_texture_half_float_linear"].forEach(function(e){var t=s.getExtension(e);t&&(r[e]=t)}),!r.OES_texture_float)return null;var i=[];i.push(e("float",s.FLOAT,Float32Array)),r.OES_texture_half_float&&i.push(e("half_float",r.OES_texture_half_float.HALF_FLOAT_OES,null));var o=s.createTexture(),n=s.createFramebuffer();s.bindFramebuffer(s.FRAMEBUFFER,n),s.bindTexture(s.TEXTURE_2D,o),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_MIN_FILTER,s.NEAREST),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_MAG_FILTER,s.NEAREST),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_WRAP_S,s.CLAMP_TO_EDGE),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_WRAP_T,s.CLAMP_TO_EDGE);for(var a=null,u=0;u<i.length;u++)if(s.texImage2D(s.TEXTURE_2D,0,s.RGBA,32,32,0,s.RGBA,i[u].type,null),s.framebufferTexture2D(s.FRAMEBUFFER,s.COLOR_ATTACHMENT0,s.TEXTURE_2D,o,0),s.checkFramebufferStatus(s.FRAMEBUFFER)===s.FRAMEBUFFER_COMPLETE){a=i[u];break}return a}(),c=function(e,t){try{return new ImageData(e,t)}catch(r){return document.createElement("canvas").getContext("2d").createImageData(e,t)}}(32,32);e("head").prepend("<style>.jquery-ripples { position: relative; z-index: 0; }</style>");var d=function(t,r){function i(){o.destroyed||(o.step(),requestAnimationFrame(i))}var o=this;this.$el=e(t),this.interactive=r.interactive,this.resolution=r.resolution,this.textureDelta=new Float32Array([1/this.resolution,1/this.resolution]),this.perturbance=r.perturbance,this.dropRadius=r.dropRadius,this.crossOrigin=r.crossOrigin,this.imageUrl=r.imageUrl;var n=document.createElement("canvas");n.width=this.$el.innerWidth(),n.height=this.$el.innerHeight(),this.canvas=n,this.$canvas=e(n),this.$canvas.css({position:"absolute",left:0,top:0,right:0,bottom:0,zIndex:-1}),this.$el.addClass("jquery-ripples").append(n),this.context=s=n.getContext("webgl")||n.getContext("experimental-webgl"),h.extensions.forEach(function(e){s.getExtension(e)}),e(window).on("resize",function(){o.updateSize()}),this.textures=[],this.framebuffers=[],this.bufferWriteIndex=0,this.bufferReadIndex=1;for(var a=h.arrayType,u=a?new a(this.resolution*this.resolution*4):null,c=0;c<2;c++){var d=s.createTexture(),f=s.createFramebuffer();s.bindFramebuffer(s.FRAMEBUFFER,f),s.bindTexture(s.TEXTURE_2D,d),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_MIN_FILTER,h.linearSupport?s.LINEAR:s.NEAREST),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_MAG_FILTER,h.linearSupport?s.LINEAR:s.NEAREST),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_WRAP_S,s.CLAMP_TO_EDGE),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_WRAP_T,s.CLAMP_TO_EDGE),s.texImage2D(s.TEXTURE_2D,0,s.RGBA,this.resolution,this.resolution,0,s.RGBA,h.type,u),s.framebufferTexture2D(s.FRAMEBUFFER,s.COLOR_ATTACHMENT0,s.TEXTURE_2D,d,0),this.textures.push(d),this.framebuffers.push(f)}this.quad=s.createBuffer(),s.bindBuffer(s.ARRAY_BUFFER,this.quad),s.bufferData(s.ARRAY_BUFFER,new Float32Array([-1,-1,1,-1,1,1,-1,1]),s.STATIC_DRAW),this.initShaders(),this.initTexture(),this.setTransparentTexture(),this.loadImage(),s.clearColor(0,0,0,0),s.blendFunc(s.SRC_ALPHA,s.ONE_MINUS_SRC_ALPHA),this.visible=!0,this.running=!0,this.inited=!0,this.destroyed=!1,this.setupPointerEvents(),requestAnimationFrame(i)};d.DEFAULTS={imageUrl:null,resolution:256,dropRadius:20,perturbance:.03,interactive:!0,crossOrigin:""},d.prototype={setupPointerEvents:function(){function e(){return r.visible&&r.running&&r.interactive}function t(t,i){e()&&r.dropAtPointer(t,r.dropRadius*(i?1.5:1),i?.14:.01)}var r=this;this.$el.on("mousemove.ripples",function(e){t(e)}).on("touchmove.ripples, touchstart.ripples",function(e){for(var r=e.originalEvent.changedTouches,i=0;i<r.length;i++)t(r[i])}).on("mousedown.ripples",function(e){t(e,!0)})},loadImage:function(){var e=this;s=this.context;var t=this.imageUrl||n(this.originalCssBackgroundImage)||n(this.$el.css("backgroundImage"));if(t!=this.imageSource)if(this.imageSource=t,this.imageSource){var r=new Image;r.onload=function(){function t(e){return 0==(e&e-1)}s=e.context;var i=t(r.width)&&t(r.height)?s.REPEAT:s.CLAMP_TO_EDGE;s.bindTexture(s.TEXTURE_2D,e.backgroundTexture),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_WRAP_S,i),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_WRAP_T,i),s.texImage2D(s.TEXTURE_2D,0,s.RGBA,s.RGBA,s.UNSIGNED_BYTE,r),e.backgroundWidth=r.width,e.backgroundHeight=r.height,e.hideCssBackground()},r.onerror=function(){s=e.context,e.setTransparentTexture()},r.crossOrigin=a(this.imageSource)?null:this.crossOrigin,r.src=this.imageSource}else this.setTransparentTexture()},step:function(){s=this.context,this.visible&&(this.computeTextureBoundaries(),this.running&&this.update(),this.render())},drawQuad:function(){s.bindBuffer(s.ARRAY_BUFFER,this.quad),s.vertexAttribPointer(0,2,s.FLOAT,!1,0,0),s.drawArrays(s.TRIANGLE_FAN,0,4)},render:function(){s.bindFramebuffer(s.FRAMEBUFFER,null),s.viewport(0,0,this.canvas.width,this.canvas.height),s.enable(s.BLEND),s.clear(s.COLOR_BUFFER_BIT|s.DEPTH_BUFFER_BIT),s.useProgram(this.renderProgram.id),o(this.backgroundTexture,0),o(this.textures[0],1),s.uniform1f(this.renderProgram.locations.perturbance,this.perturbance),s.uniform2fv(this.renderProgram.locations.topLeft,this.renderProgram.uniforms.topLeft),s.uniform2fv(this.renderProgram.locations.bottomRight,this.renderProgram.uniforms.bottomRight),s.uniform2fv(this.renderProgram.locations.containerRatio,this.renderProgram.uniforms.containerRatio),s.uniform1i(this.renderProgram.locations.samplerBackground,0),s.uniform1i(this.renderProgram.locations.samplerRipples,1),this.drawQuad(),s.disable(s.BLEND)},update:function(){s.viewport(0,0,this.resolution,this.resolution),s.bindFramebuffer(s.FRAMEBUFFER,this.framebuffers[this.bufferWriteIndex]),o(this.textures[this.bufferReadIndex]),s.useProgram(this.updateProgram.id),this.drawQuad(),this.swapBufferIndices()},swapBufferIndices:function(){this.bufferWriteIndex=1-this.bufferWriteIndex,this.bufferReadIndex=1-this.bufferReadIndex},computeTextureBoundaries:function(){var e,i=this.$el.css("background-size"),o=this.$el.css("background-attachment"),n=r(this.$el.css("background-position"));if("fixed"==o?((e={left:window.pageXOffset,top:window.pageYOffset}).width=u.width(),e.height=u.height()):((e=this.$el.offset()).width=this.$el.innerWidth(),e.height=this.$el.innerHeight()),"cover"==i)var a=Math.max(e.width/this.backgroundWidth,e.height/this.backgroundHeight),s=this.backgroundWidth*a,h=this.backgroundHeight*a;else if("contain"==i)var a=Math.min(e.width/this.backgroundWidth,e.height/this.backgroundHeight),s=this.backgroundWidth*a,h=this.backgroundHeight*a;else{var s=(i=i.split(" "))[0]||"",h=i[1]||s;t(s)?s=e.width*parseFloat(s)/100:"auto"!=s&&(s=parseFloat(s)),t(h)?h=e.height*parseFloat(h)/100:"auto"!=h&&(h=parseFloat(h)),"auto"==s&&"auto"==h?(s=this.backgroundWidth,h=this.backgroundHeight):("auto"==s&&(s=this.backgroundWidth*(h/this.backgroundHeight)),"auto"==h&&(h=this.backgroundHeight*(s/this.backgroundWidth)))}var c=n[0],d=n[1];c=t(c)?e.left+(e.width-s)*parseFloat(c)/100:e.left+parseFloat(c),d=t(d)?e.top+(e.height-h)*parseFloat(d)/100:e.top+parseFloat(d);var f=this.$el.offset();this.renderProgram.uniforms.topLeft=new Float32Array([(f.left-c)/s,(f.top-d)/h]),this.renderProgram.uniforms.bottomRight=new Float32Array([this.renderProgram.uniforms.topLeft[0]+this.$el.innerWidth()/s,this.renderProgram.uniforms.topLeft[1]+this.$el.innerHeight()/h]);var l=Math.max(this.canvas.width,this.canvas.height);this.renderProgram.uniforms.containerRatio=new Float32Array([this.canvas.width/l,this.canvas.height/l])},initShaders:function(){var e=["attribute vec2 vertex;","varying vec2 coord;","void main() {","coord = vertex * 0.5 + 0.5;","gl_Position = vec4(vertex, 0.0, 1.0);","}"].join("\n");this.dropProgram=i(e,["precision highp float;","const float PI = 3.141592653589793;","uniform sampler2D texture;","uniform vec2 center;","uniform float radius;","uniform float strength;","varying vec2 coord;","void main() {","vec4 info = texture2D(texture, coord);","float drop = max(0.0, 1.0 - length(center * 0.5 + 0.5 - coord) / radius);","drop = 0.5 - cos(drop * PI) * 0.5;","info.r += drop * strength;","gl_FragColor = info;","}"].join("\n")),this.updateProgram=i(e,["precision highp float;","uniform sampler2D texture;","uniform vec2 delta;","varying vec2 coord;","void main() {","vec4 info = texture2D(texture, coord);","vec2 dx = vec2(delta.x, 0.0);","vec2 dy = vec2(0.0, delta.y);","float average = (","texture2D(texture, coord - dx).r +","texture2D(texture, coord - dy).r +","texture2D(texture, coord + dx).r +","texture2D(texture, coord + dy).r",") * 0.25;","info.g += (average - info.r) * 2.0;","info.g *= 0.995;","info.r += info.g;","gl_FragColor = info;","}"].join("\n")),s.uniform2fv(this.updateProgram.locations.delta,this.textureDelta),this.renderProgram=i(["precision highp float;","attribute vec2 vertex;","uniform vec2 topLeft;","uniform vec2 bottomRight;","uniform vec2 containerRatio;","varying vec2 ripplesCoord;","varying vec2 backgroundCoord;","void main() {","backgroundCoord = mix(topLeft, bottomRight, vertex * 0.5 + 0.5);","backgroundCoord.y = 1.0 - backgroundCoord.y;","ripplesCoord = vec2(vertex.x, -vertex.y) * containerRatio * 0.5 + 0.5;","gl_Position = vec4(vertex.x, -vertex.y, 0.0, 1.0);","}"].join("\n"),["precision highp float;","uniform sampler2D samplerBackground;","uniform sampler2D samplerRipples;","uniform vec2 delta;","uniform float perturbance;","varying vec2 ripplesCoord;","varying vec2 backgroundCoord;","void main() {","float height = texture2D(samplerRipples, ripplesCoord).r;","float heightX = texture2D(samplerRipples, vec2(ripplesCoord.x + delta.x, ripplesCoord.y)).r;","float heightY = texture2D(samplerRipples, vec2(ripplesCoord.x, ripplesCoord.y + delta.y)).r;","vec3 dx = vec3(delta.x, heightX - height, 0.0);","vec3 dy = vec3(0.0, heightY - height, delta.y);","vec2 offset = -normalize(cross(dy, dx)).xz;","float specular = pow(max(0.0, dot(offset, normalize(vec2(-0.6, 1.0)))), 4.0);","gl_FragColor = texture2D(samplerBackground, backgroundCoord + offset * perturbance) + specular;","}"].join("\n")),s.uniform2fv(this.renderProgram.locations.delta,this.textureDelta)},initTexture:function(){this.backgroundTexture=s.createTexture(),s.bindTexture(s.TEXTURE_2D,this.backgroundTexture),s.pixelStorei(s.UNPACK_FLIP_Y_WEBGL,1),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_MAG_FILTER,s.LINEAR),s.texParameteri(s.TEXTURE_2D,s.TEXTURE_MIN_FILTER,s.LINEAR)},setTransparentTexture:function(){s.bindTexture(s.TEXTURE_2D,this.backgroundTexture),s.texImage2D(s.TEXTURE_2D,0,s.RGBA,s.RGBA,s.UNSIGNED_BYTE,c)},hideCssBackground:function(){var e=this.$el[0].style.backgroundImage;"none"!=e&&(this.originalInlineCss=e,this.originalCssBackgroundImage=this.$el.css("backgroundImage"),this.$el.css("backgroundImage","none"))},restoreCssBackground:function(){this.$el.css("backgroundImage",this.originalInlineCss||"")},dropAtPointer:function(e,t,r){var i=parseInt(this.$el.css("border-left-width"))||0,o=parseInt(this.$el.css("border-top-width"))||0;this.drop(e.pageX-this.$el.offset().left-i,e.pageY-this.$el.offset().top-o,t,r)},drop:function(e,t,r,i){s=this.context;var n=this.$el.innerWidth(),a=this.$el.innerHeight(),u=Math.max(n,a);r/=u;var h=new Float32Array([(2*e-n)/u,(a-2*t)/u]);s.viewport(0,0,this.resolution,this.resolution),s.bindFramebuffer(s.FRAMEBUFFER,this.framebuffers[this.bufferWriteIndex]),o(this.textures[this.bufferReadIndex]),s.useProgram(this.dropProgram.id),s.uniform2fv(this.dropProgram.locations.center,h),s.uniform1f(this.dropProgram.locations.radius,r),s.uniform1f(this.dropProgram.locations.strength,i),this.drawQuad(),this.swapBufferIndices()},updateSize:function(){var e=this.$el.innerWidth(),t=this.$el.innerHeight();e==this.canvas.width&&t==this.canvas.height||(this.canvas.width=e,this.canvas.height=t)},destroy:function(){this.$el.off(".ripples").removeClass("jquery-ripples").removeData("ripples"),this.$canvas.remove(),this.restoreCssBackground(),this.destroyed=!0},show:function(){this.visible=!0,this.$canvas.show(),this.hideCssBackground()},hide:function(){this.visible=!1,this.$canvas.hide(),this.restoreCssBackground()},pause:function(){this.running=!1},play:function(){this.running=!0},set:function(e,t){switch(e){case"dropRadius":case"perturbance":case"interactive":case"crossOrigin":this[e]=t;break;case"imageUrl":this.imageUrl=t,this.loadImage()}}};var f=e.fn.ripples;e.fn.ripples=function(t){if(!h)throw new Error("Your browser does not support WebGL, the OES_texture_float extension or rendering to floating point textures.");var r=arguments.length>1?Array.prototype.slice.call(arguments,1):void 0;return this.each(function(){var i=e(this),o=i.data("ripples"),n=e.extend({},d.DEFAULTS,i.data(),"object"==typeof t&&t);(o||"string"!=typeof t)&&(o?"string"==typeof t&&d.prototype[t].apply(o,r):i.data("ripples",o=new d(this,n)))})},e.fn.ripples.Constructor=d,e.fn.ripples.noConflict=function(){return e.fn.ripples=f,this}});
</script>	
<script>
	$(document).ready(function () {
      if($(document).width() >= 768){
		  $('.div2').ripples({
		  	resolution:512,
		  	dropRadius:30,
		  	perturbance:0.04
		  }); 
	  }
}); 
</script>

<div class="div3">
	<h1>ONDE TUDO COMEÇOU</h1>
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura.png" class="moldura">
	<div class="container">
		<p>Com os tempos difíceis na Sicília, Jack'O, decide deixar seu país para trás e ir à América - a terra das oportunidades - com destino a Nova York. Ele ouviu que o que não falta, são bairros criados por imigrantes com grandes oportunidades para fazer dinheiro.</p>
		<p>Ao chegar na cidade, Jack'O consegue se estabelecer e com ajuda de sua antiga "Famiglia" ele abre uma loja de bebidas. Conhecido por ser benevolente com a população, fornecendo proteção, abrigo e muitas vezes comida aos mais necessitados imigrantes, Jack'O ganha o título de Don.</p>
		<p>Os negócios estavam crescendo, mas algumas ”Famiglias” como a de Al Gatone não estavam felizes com o prosperar de Jack'O. Obrigando-o a criar novas oportunidades para sua ”Famiglia”.</p>
		<p>Sem opções aparente, Don Jack'O decide mudar o rumo de seus negócios ao conversar com um dos moradores, um feirante local que devia a vida ao Don, cujo primo era dono de uma fazenda de limões.</p>
		<p>Após visitar o fazendeiro que morava na Califórnia e experimentar uma de suas limonadas, Don Jack'O viu uma oportunidade e decidiu comercializar a bebida. Com alguns ajustes na fórmula, o Don encontrou o frescor e sabor inigualáveis. De acordo com ele, era necessário saber onde "apertar" para tirar o verdadeiro sabor dos limões.</p>
	</div>

	<div class="botao" data-toggle="modal" data-target="#myModal">
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/botao.png" class="moldura-botao">
		<p class="">IDEALIZADORES</p>
	</div>

	<!-- Modal Idealizadores -->
	<div class="modal fade" id="myModal" role="dialog">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content modal-idealizadores">
	    	<div class="">
	          <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
	        </div>
	    	<h1>Idealizadores</h1>
	    	<div class="row">
	    		<div class="col-sm-4">
	    			<div class="img-idealizador"></div>
	    			<p>Nome do Idealizador</p>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="img-idealizador"></div>
	    			<p>Nome do Idealizador</p>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="img-idealizador"></div>
	    			<p>Nome do Idealizador</p>
	    		</div>
	    	</div>
	    </div>
	  </div>
	</div>

</div>

<div class="div4">
	<div class="container">
		<h1>ONDE ENCONTRAR</h1>
		<p>Nossa proposta é trazer ao seu negóciouma limonada refrescante, saborosa e de alta qualidade. Conheça os estabelecimentos que já são nossos parceiros e experimente nosso sabor inconfudivel.</p>
		<p>Caso queira fazer parte da nossa "famiglia", <a href="">clique aqui</a></p>

		<div class="row">
			<div class="col-sm-3">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura2.png" class="moldura-local">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14615.917339103991!2d-46.535112!3d-23.676697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddbc02a7b569959b!2sGrano+Studio!5e0!3m2!1spt-BR!2sbr!4v1519659877158" width="180" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
				<h3>NOME DO ESTABELECIMENTO</h3>
				<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/localizacao.png" class="localizacao"><a href="https://www.google.com/maps?ll=-23.676697,-46.535112&z=14&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=15977648497330066843">Como chegar</a></p>
			</div>
			
			<div class="col-sm-3">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura2.png" class="moldura-local">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14615.917339103991!2d-46.535112!3d-23.676697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddbc02a7b569959b!2sGrano+Studio!5e0!3m2!1spt-BR!2sbr!4v1519659877158" width="180" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
				<h3>NOME DO ESTABELECIMENTO</h3>
				<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/localizacao.png" class="localizacao"><a href="https://www.google.com/maps?ll=-23.676697,-46.535112&z=14&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=15977648497330066843">Como chegar</a></p>
			</div>

			<div class="col-sm-3">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura2.png" class="moldura-local">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14615.917339103991!2d-46.535112!3d-23.676697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddbc02a7b569959b!2sGrano+Studio!5e0!3m2!1spt-BR!2sbr!4v1519659877158" width="180" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
				<h3>NOME DO ESTABELECIMENTO</h3>
				<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/localizacao.png" class="localizacao"><a href="https://www.google.com/maps?ll=-23.676697,-46.535112&z=14&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=15977648497330066843">Como chegar</a></p>
			</div>

			<div class="col-sm-3">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura2.png" class="moldura-local">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14615.917339103991!2d-46.535112!3d-23.676697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddbc02a7b569959b!2sGrano+Studio!5e0!3m2!1spt-BR!2sbr!4v1519659877158" width="180" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
				<h3>NOME DO ESTABELECIMENTO</h3>
				<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/localizacao.png" class="localizacao"><a href="https://www.google.com/maps?ll=-23.676697,-46.535112&z=14&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=15977648497330066843">Como chegar</a></p>
			</div>			
		</div>

		<div class="botao" data-toggle="modal" data-target="#myModal-vejatodos">
			<img src="<?php echo get_stylesheet_directory_uri();?>/img/botao.png" class="moldura-botao">
			<p class="">VEJA TODOS</p>
		</div>

		<!-- Modal Veja Todos -->
		<div class="modal fade" id="myModal-vejatodos" role="dialog">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content modal-vejatodos">
		    	<div class="">
		          <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
		        </div>
		    	<h1>Nossos Locais</h1>
		    	<div class="row">
					<div class="col-sm-3">
						<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura2.png" class="moldura-local">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14615.917339103991!2d-46.535112!3d-23.676697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddbc02a7b569959b!2sGrano+Studio!5e0!3m2!1spt-BR!2sbr!4v1519659877158" width="180" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
						<h3>NOME DO ESTABELECIMENTO</h3>
						<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/localizacao.png" class="localizacao"><a href="https://www.google.com/maps?ll=-23.676697,-46.535112&z=14&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=15977648497330066843">Como chegar</a></p>
					</div>
					
					<div class="col-sm-3">
						<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura2.png" class="moldura-local">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14615.917339103991!2d-46.535112!3d-23.676697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddbc02a7b569959b!2sGrano+Studio!5e0!3m2!1spt-BR!2sbr!4v1519659877158" width="180" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
						<h3>NOME DO ESTABELECIMENTO</h3>
						<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/localizacao.png" class="localizacao"><a href="https://www.google.com/maps?ll=-23.676697,-46.535112&z=14&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=15977648497330066843">Como chegar</a></p>
					</div>

					<div class="col-sm-3">
						<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura2.png" class="moldura-local">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14615.917339103991!2d-46.535112!3d-23.676697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddbc02a7b569959b!2sGrano+Studio!5e0!3m2!1spt-BR!2sbr!4v1519659877158" width="180" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
						<h3>NOME DO ESTABELECIMENTO</h3>
						<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/localizacao.png" class="localizacao"><a href="https://www.google.com/maps?ll=-23.676697,-46.535112&z=14&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=15977648497330066843">Como chegar</a></p>
					</div>

					<div class="col-sm-3">
						<img src="<?php echo get_stylesheet_directory_uri();?>/img/moldura2.png" class="moldura-local">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14615.917339103991!2d-46.535112!3d-23.676697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddbc02a7b569959b!2sGrano+Studio!5e0!3m2!1spt-BR!2sbr!4v1519659877158" width="180" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
						<h3>NOME DO ESTABELECIMENTO</h3>
						<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/localizacao.png" class="localizacao"><a href="https://www.google.com/maps?ll=-23.676697,-46.535112&z=14&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=15977648497330066843">Como chegar</a></p>
					</div>	
				</div>
		    </div>
		  </div>
		</div> 

	</div>	
</div>

<div class="div5">
	<div class="col-sm-6 insta-fotos">		
	
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/insta_lemonator1.jpeg" class="">	
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/insta_lemonator2.jpeg" class="">
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/insta_lemonator3.jpeg" class="">
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/insta_lemonator4.jpeg" class="">
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/insta_lemonator5.jpeg" class="">
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/insta_lemonator6.jpeg" class="">
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/insta_lemonator7.jpeg" class="">
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/insta_lemonator8.jpeg" class="">
 	
	</div>
	<div class="col-sm-6">
		<h1>INSTAGRAM</h1>
		<p>Acompanhe nossas fotos e junte-se a "famiglia</p>
		<a href="https://www.instagram.com/thelemonatorsp/?hl=pt-br">	
			<div class="botao">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/botao.png" class="moldura-botao">
				<p class="">SIGA-NOS NO INSTAGRAM!</p>
			</div>
		</a>

	</div>	
</div>

<div class="div6">
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/limao-rodela.png" class="limao-rodela-div6">
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/logo.png" class="img-logo-div6">
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/fundo-header-cortada.png" class="fundo-header-div6">


	<h1>TENHA O NOSSO SABOR<br> REFRESCANTE E IRRESISTÍVEL EM<br>  SEU ESTABELECIMENTO</h1>
	<p class="pos-titulo-div6">Preencha os dados, entraremos em contato</p>

	<img src="<?php echo get_stylesheet_directory_uri();?>/img/limao-rodela-metade.png" class="limao-rodela-div6-2">
	<img src="<?php echo get_stylesheet_directory_uri();?>/img/fundo-header-cortada2.png" class="fundo-header-div6-2">	

	<div class="contato container">
		<?php echo do_shortcode('[contact-form-7 id="12" title="Contato"]'); ?>
		<p>desenvolvido por <a href="http://granostudio.com.br/">GranoStudio</a></p>		
	</div>

	<img src="<?php echo get_stylesheet_directory_uri();?>/img/limao-rodela2.png" class="limao-rodela-div6-3">

</div>

<?php get_footer(); ?>

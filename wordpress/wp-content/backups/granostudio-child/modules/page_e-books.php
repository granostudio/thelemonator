<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<div class="tituloI titulo">
  <h1>E-Books</h1>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-5 col-sm-offset-1">
      <div class="texto_conteudo">
        <h2 style="text-transform: inherit;margin-bottom:25px;">Gestão em Sáude - E-Book</h2>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ac metus velit. Nulla sagittis facilisis nulla sed tincidunt. Nam quam sapien, rhoncus at arcu ut, imperdiet molestie velit.
          Praesent vel sem nunc. Duis sodales bibendum lectus, mattis ornare risus pellentesque vitae. Nam nisi tellus, euismod at risus at, porta fringilla elit. Vivamus sodales quam vitae risus vulputate, eu vestibulum risus lobortis.
          Donec eget egestas sapien. Etiam ultricies pellentesque condimentum.</p>

        <h3 style="margin-bottom: 35px;">Cadastre-se para ter acesso ao E-Book</h3>

        <?php echo do_shortcode ('[contact-form-7 id="483" title="E-Book"]'); ?>

        <?php $args = array( 'post_type' => 'Landing Page');
        $loop = new WP_Query( $args );
          while ( $loop->have_posts() ) : $loop->the_post(); ?>

          <?php $file = get_post_meta( get_the_ID(), 'wiki_test_image', true ); ?>

        <?php endwhile; ?>

        <!-- Botão que faz o download do arquivo -->
        <!-- <a href="<?php echo $file ?>" type="submit" download class="btn btn-primmary" target="_blank">Download</a> -->

      </div>
    </div>

    <div class="col-sm-6 img_ebook">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/e-book.png">
    </div>

  </div>
</div>
<?php get_footer(); ?>

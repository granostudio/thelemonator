<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<div class="tituloI titulo">
  <h1>Conteúdos Exclusívos</h1>
</div>

<div class="div_conteudo">
  <div class="row_conteudo">
      <div class="coluna3">
        <div class="container">
          <div class="texto_conteudo">
            <h3>ACESSE NOSSOS CONTEÚDOS EXCLUSÍVOS</h3>
            <form class="" action="" method="post">
              <div class="input-group">
                  <input type="nome" name="nome" placeholder="Nome Completo" class="form-teste"><br>
                  <input type="email" name="e-mail" placeholder="E-mail" class="form-teste"><br>
                  <a href="" class="btn btn-primmary">Acessar</a>
              </div>
            </form>

          </div>
        </div>
      </div>
      <div class="coluna-direita_conteudo col2 hidden-xs">
        <div class="fundo_conteudo"></div>
      </div>
  </div>
</div>

</div>


<?php get_footer(); ?>

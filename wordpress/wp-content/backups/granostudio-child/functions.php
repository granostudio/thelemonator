<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */




/**
 * Enqueues scripts and styles.
 *
 */

 /*
 * Creating a function to create our CPT
 */

//======================== Custom post type para video ========================//

 function custom_post_type() {

 // Set UI labels for Custom Post Type
 	$labels = array(
 		'name'                => _x( 'Movies', 'Post Type General Name', 'twentythirteen' ),
 		'singular_name'       => _x( 'Movie', 'Post Type Singular Name', 'twentythirteen' ),
 		'menu_name'           => __( 'Movies', 'twentythirteen' ),
 		'parent_item_colon'   => __( 'Parent Movie', 'twentythirteen' ),
 		'all_items'           => __( 'All Movies', 'twentythirteen' ),
 		'view_item'           => __( 'View Movie', 'twentythirteen' ),
 		'add_new_item'        => __( 'Add New Movie', 'twentythirteen' ),
 		'add_new'             => __( 'Add New', 'twentythirteen' ),
 		'edit_item'           => __( 'Edit Movie', 'twentythirteen' ),
 		'update_item'         => __( 'Update Movie', 'twentythirteen' ),
 		'search_items'        => __( 'Search Movie', 'twentythirteen' ),
 		'not_found'           => __( 'Not Found', 'twentythirteen' ),
 		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
 	);

 // Set other options for Custom Post Type

 	$args = array(
 		'label'               => __( 'movies', 'twentythirteen' ),
 		'description'         => __( 'Movie news and reviews', 'twentythirteen' ),
 		'labels'              => $labels,
 		// Features this CPT supports in Post Editor
 		'supports'            => array( 'title', 'editor', 'thumbnail' ),
 		// You can associate this CPT with a taxonomy or custom taxonomy.
 	// 	'taxonomies'          => array( 'genres' ),
 		/* A hierarchical CPT is like Pages and can have
 		* Parent and child items. A non-hierarchical CPT
 		* is like Posts.
 		*/
 		'hierarchical'        => false,
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_nav_menus'   => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => 1,
 		'can_export'          => true,
 		'has_archive'         => true,
 		'exclude_from_search' => false,
 		'publicly_queryable'  => true,
 		'capability_type'     => 'page',
 	);

 	// Registering your Custom Post Type
 	register_post_type( 'movies', $args );

 }

 /* Hook into the 'init' action so that the function
 * Containing our post type registration is not
 * unnecessarily executed.
 */


//======================== Custom post type para Landing page ========================//
 // Set UI labels for Custom Post Type
 	$labelsLandingPage = array(
 		'name'                => _x( 'Landing Page', 'Post Type General Name', 'twentythirteen' ),
 		'singular_name'       => _x( 'Landing Page', 'Post Type Singular Name', 'twentythirteen' ),
 		'menu_name'           => __( 'Landing Page', 'twentythirteen' ),
 		'parent_item_colon'   => __( 'Parent Landing Page', 'twentythirteen' ),
 		'all_items'           => __( 'Todas Landing Page', 'twentythirteen' ),
 		'view_item'           => __( 'View Landing Page', 'twentythirteen' ),
 		'add_new_item'        => __( 'Adicionar nova Landing Page', 'twentythirteen' ),
 		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
 		'edit_item'           => __( 'Editar Landing Page', 'twentythirteen' ),
 		'update_item'         => __( 'Update Landing Page', 'twentythirteen' ),
 		'search_items'        => __( 'Compartilhar Landing Page', 'twentythirteen' ),
 		'not_found'           => __( 'Not Found', 'twentythirteen' ),
 		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
 	);

 // Set other options for Custom Post Type

 	$argsLandingPage = array(
 		'label'               => __( 'Landing Page', 'twentythirteen' ),
 		'description'         => __( 'Landing Page news and reviews', 'twentythirteen' ),
 		'labels'              => $labelsLandingPage,
 		// Features this CPT supports in Post Editor
 		'supports'            => array( 'title', 'editor', 'thumbnail' ),
 		// You can associate this CPT with a taxonomy or custom taxonomy.
 	// 	'taxonomies'          => array( 'genres' ),
 		/* A hierarchical CPT is like Pages and can have
 		* Parent and child items. A non-hierarchical CPT
 		* is like Posts.
 		*/
 		'hierarchical'        => false,
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_nav_menus'   => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => 5,
 		'can_export'          => true,
 		'has_archive'         => true,
 		'exclude_from_search' => false,
 		'publicly_queryable'  => true,
 		'capability_type'     => 'page',
 	);

 	// Registering your Custom Post Type
 	register_post_type( 'landingpage', $argsLandingPage );


 add_action( 'init', 'custom_post_type', 0 );

//======================== Custom post type para video ========================//


//=========================== Custom_post_type com o plugin CMB2 ===========================//

add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_yourprefix_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Opções', 'cmb2' ),
        'object_types'  => array( 'movies', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    // URL text field
    $cmb->add_field( array(
        'name' => __( 'URL do vídeo', 'cmb2' ),
        'desc' => __( 'Digite a URL do vídeo', 'cmb2' ),
        'id'   => '_url',
        'type' => 'text_url',
        // 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
        // 'repeatable' => true,
    ) );

    // Start with an underscore to hide fields from custom fields list
    // $prefix = 'e-books';

    /**
    * Initiate the metabox
    */
    $cmb = new_cmb2_box( array(
      'id'            => 'Landing Page',
      'title'         => __( 'Opções', 'cmb2' ),
      'object_types'  => array( 'landingpage', ), // Post type
      'context'       => 'normal',
      'priority'      => 'high',
      'show_names'    => true, // Show field names on the left
      // 'cmb_styles' => false, // false to disable the CMB stylesheet
      // 'closed'     => true, // Keep the metabox closed by default
      ) );

      // // Email text field
      // $cmb->add_field( array(
      //   'name' => __( 'Test Text Email', 'cmb2' ),
      //   'desc' => __( 'field description (optional)', 'cmb2' ),
      //   'id'   => 'E-Books' . 'email',
      //   'type' => 'text_email',
      //   // 'repeatable' => true,
      //   ) );

        $cmb->add_field( array(
          'name'    => 'Adicionar Arquivo',
          'desc'    => 'Adicionar arquivo em PDF, E-Books e imagens.',
          'id'      => 'wiki_test_image',
          'type'    => 'file',
          // Optional:
          'options' => array(
              'url' => false, // Hide the text input for the url
          ),
          'text'    => array(
              'add_upload_file_text' => 'Adicionar Arquivo' // Change upload button text. Default: "Add or Upload File"
          ),
          // query_args are passed to wp.media's library query.
          'query_args' => array(
              'type' => 'application/pdf', // Make library only display PDFs.
          ),

          ) );

}







//=========================== Custom_post_type com o plugin CMB2 ===========================//

function granostudio_scripts_child() {

	//Desabilitar jquery
	wp_deregister_script( 'jquery' );

	// Theme stylesheet.
	wp_enqueue_style( 'granostudio-style', get_stylesheet_uri()  );
	// import fonts (Google Fonts)
	wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
	// Theme front-end stylesheet
	wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.min.css');

	// scripts js
	wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', '000001', false, true);
	// network effet home
	  if (is_front_page()){
			wp_enqueue_script('granostudio-network', get_stylesheet_directory_uri() . '/js/network-ani.js', '000001', false, true);
	  }
}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );

// Removendo o jetpack do fundo do post
function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display', 19 );
    remove_filter( 'the_excerpt', 'sharing_display', 19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}

add_action( 'loop_start', 'jptweak_remove_share' );


function redir_after_form_sent($form) {
      $id = url_to_postid( "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" );
      $arquivourl = wp_get_attachment_url( get_post_meta( $id , 'wiki_test_image_id', 1 ) );

      if ($form->id() == 483) {
        echo '<script> window.location.href="'.$arquivourl.'";</script>';
      }
}
add_action('wpcf7_mail_sent', 'redir_after_form_sent');
?>

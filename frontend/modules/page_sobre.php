<?php get_header(); ?>

<!-- Div banner -->
<div class="div1-sobre">
  <div class="col-md-10 col-md-offset-1">
    <h1>O Grupo Sistran está presente em 15 países da América Latina e nos Estados Unidos</h1>
    <p>No Brasil, a Sistran processa um terço de todos os prêmios de seguro de Vida e acumula cerca de 30 anos de história local.</p>
    <p>Muito além do simples emprego da tecnologia, a Sistran trabalha continuamente no aperfeiçoamento de funcionalidades em benefício dos usuários finais.
      Para isso, utiliza conhecimento de mais de 160 especialistas no Brasil e 600 na América Latina para entregar soluções de negócio com alto indice de satisfação do cliente, por meio de produtos próprios ou de marcas globais.
    </p>
    <div class="row" style="margin-top: 50px;">
      <div class="col-sm-4">
        <button type="button" class="botao botao-home" name="button" data-toggle="modal" data-target=".bd-example-modal-lg1">Seguradoras de Grande Porte</button>
      </div>
      <div class="col-sm-4">
        <button type="button" class="botao botao-home" name="button" data-toggle="modal" data-target=".bd-example-modal-lg2">Seguradoras Internacionais</button>
      </div>
      <div class="col-sm-4">
        <button type="button" class="botao botao-home" name="button" data-toggle="modal" data-target=".bd-example-modal-lg3">Seguradoras de Nicho</button>
      </div>
    </div>
  </div>

  <!-- Modal1 -->
  <div class="modal fade bd-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="">
          <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
        </div>
        <h1>Seguradoras de Grande Porte</h1>
        <hr class="titulo">
        <div class="banner-modal1"></div>
        <div class="conteudo-modal">
          <h3>Dedicada ao mercado segurador, com eperiência em todos os ramos, a Sistran atua como integradora de sistemas para clientes com grandes carteiras;</h3>
          <li><p>Maior empresa de TI 100% especializada no segmento de Seguros no Brasil, com cerca de 30 anos de experiência e lições aprendidas em mais de 30 implementações de ERP bem-sucedidas;</li>
          <li>
            <ul>Serviços de consultoria:
              <li>Diagnóstico;</li>
              <li>Seleção de ERP;</li>
              <li>Desenho de integração de aplicações;</li>
              <li>Gerenciamento de projetos;</li>
              <li>Redesenho e gerenciamento de processos (BPM);</li>
              <li>Governança de TI;</li>
            </ul>
          </li>
          <li><p>Especialistas em Seguros com efetivo conhecimento da realidade local e profunda capacidade de análise das funcionalidades de negócio e legais (normativas Susep), assegurando precisão na definição de escopo, garantindo aderência às necessidades de negócio, evitando retrabalho e minimizando risco de insucesso do projeto (tempos e custos adicionais);</p></li>
          <li><p>Desenvolvimento de projetos sob medida, gerenciados por métricas formais (pontos de função) e SLA's, totalizando 300 mil horas/ano. Projetos e serviços auditados 100% pelo PMO com forte compromisso em prazos, custos e qualidade;</p></li>
          <li><p>Porte e padrão de qualidade internacional com custo de fornecedor local. Melhores práticas nacionais (negócios) e internacionais (TI) para Seguradoras, associada às metodologias e certificações ISO:9001, CMMi, ÁGIL e PMO;</p></li>
          <li><p>Expertise em integração com sistemas legados e migrações de bases de dados, comprovadas por meio do histórico de mais de 30 casos de sucesso, garantem a segurança do contratante;</p></li>
          <li><p>Soluções robustas e comprovadas que suportam o processamento de milhões de apólices, aliando performance e flexibilidade. Software completo para automação de seguradoras, com módulos independentes. Arquitetura de negócio (Bankassurance, Corretores e Parceiros) pronta, testada e aprovada, permitindo lançamento rápido de produtos (3 dias);</p></li>
          <li><p>Uso de componentes aceleradores (serviços e objetos) para entregar projeto 100% customizado ao cliente em curto prazo;</p></li>
          <li><p>Parcerias com empresas globais, combinando nosso conhecimento de negócio com tecnologias para transformação digital: omnichannel, business intelligence, big data, machine learning, robótica (RPA), BPM, ECM (gestão de documentos e workflow) e CRM, sempre com foco no CX (customer experience);</p></li>
          <li><p>Modelo comercial flexível e escalável (CAPEX/OPEX). Possibilidade de modelo SaaS/alugel/venda da Licença de uso ou disponibilização de Código Fonte (ou ainda escrow).</p></li>

          <p></p> 
          <p><a href="" data-dismiss="modal" class="esc-modal">Voltar</a></p>

        </div>
      </div>
    </div>
  </div>

  <!-- Large modal2 -->
  <div class="modal fade bd-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="">
          <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
        </div>
        <h1>Seguradoras Internacionais</h1>
        <hr class="titulo">
        <div class="banner-modal2"></div>
        <div class="conteudo-modal">
          <li><p>Tradicional fornecedora de soluções de seguros para a América Latina, com presença em 14 países da região, além de Brasil e Estados Unidos; mais de 130 clientes, 200 implantações de ERP e 600 colaboradores na região;</p></li>
          <li>
            <ul>Soluções testadas e aprovadas que suportam o processamento regional, multimoeda, multisseguradora, multilíngua, compatíveis com requisitos dos países da região;
              <li>Consolidam operações com processamento regional;</li>
              <li>Altamente parametrizáveis;</li>
              <li>Ótima relação custo-benefício;</li>
            </ul>
          </li>
          <li><p>Parcerias com empresas globais, combinando nosso conhecimento de negócio com tecnologias para transformação digital: omnichannel, business intelligence, big data, machine learning, robótica (RPA), BPM, ECM (gestão de documentos e workflow) e CRM, sempre com foco no CX (customer experience);</p></li>
          <li><p>Contratos com SLA para atendimento regional;</p></li>
          <li><p>Melhores práticas nacionais e internacionais (negócios e TI) para Seguradoras, associada às metodologias e certificações PMO, IS0:9001, CMMi e ÁGIL;</p></li>
          <li><p>Expertise em projetos de integração, complementada pela experiência em migrações de empresas e sistemas;</p></li>
          <li><p>Modelo comercial flexível e escalável (CAPEX/ OPEX). Possibilidade de modelo SaaS/aluguel /venda da Licença de uso ou disponibilização do Código Fonte (ou ainda escrow);</p></li>
          <li><p>BPO.</p></li>

          <p></p> 
          <p><a href="" data-dismiss="modal" class="esc-modal">Voltar</a></p>

        </div>
      </div>
    </div>
  </div>

  <!-- Large modal3 -->
  <div class="modal fade bd-example-modal-lg3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="">
          <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
        </div>
        <h1>Seguradoras de Nicho</h1>
        <hr class="titulo">
        <div class="banner-modal3"></div>
        <div class="conteudo-modal">
          <li><p>Soluções prontas para implantação acelerada, incluindo serviços de customização para refletir as necessidades do seu negócio e normativas legais;</p></li>
          <li><p>SaaS (Software as a Service) para os módulos e BPO de processos (Emissões, Resseguros e Sinistros), trazendo maior flexibilidade financeira e operacional. Modelo comercial flexível e escalável (CAPEX/ OPEX);</p></li>
          <li><p>Serviços de consultoria:</p></li>
          <ul>
            <li>Diagnóstico;</li>
            <li>Seleção de ERP;</li>
            <li>Desenho de integração de aplicações;</li>
            <li>Gerenciamento de projetos;</li>
            <li>Redesenho e gerenciamento de processos (BPM);</li>
            <li>Governança de TI;</li>
          </ul>

          <li><p>Alocação de mão de obra especializada;</p></li>
          <li><p>Expertise em projetos de integração, complementada pela experiência em migrações de empresas e sistemas (M&A);</p></li>
          <li><p>Projetos de desenvolvimento sob medida;</p></li><br>
          <li><p>Projetos de business intelligence e big data para empresas de nicho.</p></li>

          <p></p> 
          <p><a href="" data-dismiss="modal" class="esc-modal">Voltar</a></p>
          
        </div>
      </div>
    </div>
  </div>

</div>
<!-- Fim div1 -->

<!-- Div Mapa --> 
<div class="div-mapa">
  <div class="col-sm-8 col-lg-7 col-lg-offset-1 mapa-brasil">
    <div class="pulse"><div class="tooltip-mapa">R. Haddock Lobo, 356 - Tijuca, Rio de Janeiro - RJ, 20260-142</div></div>
    <div class="pulse"><div class="tooltip-mapa">R. Luigi Galvani, 70 - Itaim Bibi, São Paulo - SP, 04575-020</div></div>
    <div class="bullet"></div><!-- Nº3 -->
    <div class="bullet"></div><!-- Nº4 -->
    <div class="bullet"></div><!-- Nº5 -->
    <div class="bullet"></div><!-- Nº6 -->
    <div class="bullet"></div><!-- Nº7 -->
    <div class="bullet"></div><!-- Nº8 -->
    <div class="bullet"></div><!-- Nº9 -->
    <div class="bullet"></div><!-- Nº10 -->
    <div class="bullet"></div><!-- Nº11 -->
    <div class="bullet"></div><!-- Nº12 -->
    <div class="bullet"></div><!-- Nº13 -->
    <div class="bullet"></div><!-- Nº14 -->
    <div class="bullet"></div><!-- Nº15 -->
    <div class="col-sm-5">
      <div class="row">
        <h3>Grupo Sistran</h3>
        <hr class="titulo">
      </div>

      <div class="row">
        <div class="imagem"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Local.svg"></div>
        <p><span class="titulo-span">Escritório</span><span style="font-size: 15px;color: #15427D;font-weight: unset;"><br>Presença em 15 países da América Latina,<br> mais os Estados Unidos</span></p>
      </div>

      <div class="row">
        <div class="imagem"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Colaboradores.svg"></div>
        <p><span class="titulo-span">Colaboradores</span><br>+<span class="count">600</span></p>
      </div>

      <div class="row">
        <div class="imagem"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-ERP.svg"></div>
        <p><span class="titulo-span">Implementação de ERPs</span><br>+<span class="count">200</span></p>
      </div>

      <div class="row">
        <div class="imagem"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Cliente.svg"></div>
        <p><span class="titulo-span">Clientes</span><br>+<span class="count">130</span></p><br>
        <p class="continuar">Continuar para<br><a href="http://www.sistran.com.ar" target="_blank">Sistran Latam</a></p>
      </div>

    </div>
  </div>

  <div class="col-sm-4">
    <div class="row">
      <h1>Sistran Brasil</h1>
      <hr class="titulo">
    </div>

    <div class="row">
      <div class="imagem"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Local.svg"></div>
      <p><span class="titulo-span">Escritório</span><br>São Paulo e Rio de Janeiro</p>
    </div>
 
    <div class="row">
      <div class="imagem"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Colaboradores.svg"></div>
      <p><span class="titulo-span">Colaboradores</span><br><span class="span160">+<span class="count">160</span></span></p>
        <div class="grafico"><span class="span1"><span class="count">62</span>%</span><img src="<?php echo get_stylesheet_directory_uri();?>/img/62-graph.png"><p>Com mais de 15 anos de experiência</p></div>
        <div class="grafico"><span class="span1"><span class="count">50</span>%</span><img src="<?php echo get_stylesheet_directory_uri();?>/img/50-graph.png"><p>há mais de seis anos na Sistran Brasil</p></div>
    </div>

    <div class="row">
      <div class="imagem"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-ERP.svg"></div>
      <p><span class="titulo-span">Implementação de ERPs</span><br><span class="span160">+<span class="count">30</span></span></p>
    </div>

    <div class="row">
      <div class="imagem"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Cliente.svg"></div>
      <p><span class="titulo-span">Clientes</span><br><span class="span160">+<span class="count">25</span></span></p>
    </div>
  </div>
</div>
<!-- Fim Div Mapa -->

<!-- Div Sobre -->
<div class="div2-sobre">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3" style="margin-bottom: 30px;">
        <h1>Sobre a Sistran</h1>
        <hr class="titulo">
      </div>
    </div>

    <div class="col-sm-6">
      <h2>A Sistran Brasil iniciou atividades em 1988 com o objetivo de fornecer projetos de tecnologia para processamento de informações e interação com os canais corretores.</h2>
      <p>
         Ao longo de seus cerca de 30 anos de história, o peso do mercado segurador no Produto Interno Bruto (PIB) passou de 1% para aproximadamente 5%.
         Esse crescimento veio acompanhado de uma maior complexidade das demandas: os clientes precisavam de parceiros capazes de fornecer serviços consultivos, especializados e que realmente falassem a língua de Seguros.
      </p>
    </div>
    <div class="col-sm-4 atividades">
      <h4>A Sistran Brasil se destacou dos demais players graças à sua abordagem completa, que abrange:</h4>
      <div class="row">
        <div class="col-sm-3"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-tecnologias-proprias.svg"></div><div class="col-sm-9"><p>Tecnologias próprias;</p></div>
      </div>
      <div class="row">
        <div class="col-sm-3"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Integracao.svg"></div><div class="col-sm-9"><p>Integração de tecnologias de parceiros;</p></div>
      </div>
      <div class="row">
        <div class="col-sm-3"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-projetos.svg"></div><div class="col-sm-9"><p>Consultoria;</p></div>
      </div>
      <div class="row">
        <div class="col-sm-3"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Servicos.svg"></div><div class="col-sm-9"><p>Serviços: body shop, desenvolvimento, especificação funcional, entre outros.</p></div>
      </div>
    </div>



  </div>
</div>
<!-- Fim div2 -->

<!-- Div diferenciais -->
<div class="div3-sobre">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h1>Diferenciais</h1>
        <hr class="titulo">
      </div>
    </div>
    <div class="row">
      <div class="col-sm-2 col-sm-offset-2 diferenciais div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-diferenciais-1.svg">
        <p class="paragrafo-div">Especialização em Seguradoras</p>
      </div>
      <div class="col-sm-2 col-sm-offset-1 diferenciais div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-diferenciais-2.svg">
        <p class="paragrafo-div">Atendimento de nível global, custo local</p>
      </div>
      <div class="col-sm-2 col-sm-offset-1 diferenciais div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-diferenciais-3.svg">
        <p class="paragrafo-div">Estrutura sólida e perene: mais de 30 anos de vida</p>
      </div>
    </div>

    <div class="row">  
      <div class="col-sm-2 col-sm-offset-2 diferenciais div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-diferenciais-4.svg">
        <p class="paragrafo-div">Processa 1/3 dos prêmios de Seguro de Vida no Brasil</p>
      </div>
      <div class="col-sm-2 col-sm-offset-1 diferenciais div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-diferenciais-5.svg">
        <p class="paragrafo-div">Conhecimento de regulações da Susep</p>
      </div>
      <div class="col-sm-2 col-sm-offset-1 diferenciais div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-diferenciais-6.svg">
        <p class="paragrafo-div">Metodologias e frameworks mundiais</p>
      </div>
      <div class="col-sm-12">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-iso.png">
      </div>
    </div>

  </div>
</div>
<!-- Fim div3 -->

<!-- Div Abordagem -->
<div class="div4-sobre">
  <div class="container">
    <div class="col-sm-8 col-sm-offset-2 div-fade">
      <h1>Temos uma abordagem completa de projetos para o mercado Segurador:</h1>
    </div>
    <div class="row">
      <div class="col-sm-2 col-sm-offset-2 div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-negocios.svg">
        <p class="paragrafo-div">Consultoria de negócios</p>
      </div>
      <div class="col-sm-2 div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-lupa.svg">
        <p class="paragrafo-div">Análise funcional</p>
      </div>
      <div class="col-sm-2 div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-projetos.svg">
        <p class="paragrafo-div">Projetos</p>
      </div>
      <div class="col-sm-2 div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-Outsourcing.svg">
        <p class="paragrafo-div">Outsourcing</p>
      </div>

    </div>
  </div>
</div>
<!-- Fim div4 -->

<!-- Div banner -->
<div class="div5-sobre">
  <div class="banner">

    <div class="carousel slide article-slide" id="article-photo-carousel">

  <!-- Wrapper for slides -->
      <div class="carousel-inner cont-slider">

        <div class="item active">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/img-banner-solucoes-negocios-1.jpg">
          <div class="titulo-banner">
            <h1>Soluções de Negócio</h1>
            <hr class="titulo" style="width: 55%;">
          </div>
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/sistran-banner-2.jpg">
          <div class="titulo-banner">
            <h1>Soluções de Negócio</h1>
            <hr class="titulo" style="width: 55%;">
          </div>
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/sistran-banner.jpg">
          <div class="titulo-banner">
            <h1>Soluções de Negócio</h1>
            <hr class="titulo" style="width: 55%;">
          </div>
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/img-banner-solucoes-negocios-4.jpg">
          <div class="titulo-banner">
            <h1>Soluções de Negócio</h1>
            <hr class="titulo" style="width: 55%;">
          </div>
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/img-banner-solucoes-negocios-5.jpg">
          <div class="titulo-banner">
            <h1>Soluções de Negócio</h1>
            <hr class="titulo" style="width: 55%;">
          </div>
        </div>
      </div>
      <!-- Indicators -->
      <ol class="carousel-indicators col-sm-6">
        <li class="active" data-slide-to="0" data-target="#article-photo-carousel"><p>Vida</p></li>
        <li class="" data-slide-to="1" data-target="#article-photo-carousel"><p>Previdência</p></li>
        <li class="" data-slide-to="2" data-target="#article-photo-carousel"><p>Ramos Elementares</p></li>
        <li class="" data-slide-to="3" data-target="#article-photo-carousel"><p>Auto</p></li>
        <li class="" data-slide-to="4" data-target="#article-photo-carousel"><p>Resseguros</p></li>
      </ol>
    </div>

  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h1>Premiações</h1>
        <hr class="titulo" style="width: 15%;">
      </div>
    </div>

    <div class="row col-sm-10 col-sm-offset-1">
      <h2>Em mais de 30 anos de história, a Sistran Brasil conquistou diversos prêmios pelo resultado de seus projetos e, também, por reconhecimento de parceiros internacionais:</h2>
    </div>
    <div class="col-sm-4 col-md-3 col-sm-offset-2 col-md-offset-3">
      <div class="row"><div class="n-premiacao col-sm-4"><p>8</p></div><div class="premiacao col-sm-6">Gaivotas de Ouro</div></div>
      <div class="row"><div class="n-premiacao col-sm-4"><p>5</p></div><div class="premiacao col-sm-6">Reconhecimentos internacionais</div></div>
    </div>
    <div class="col-sm-4 col-md-3">
      <div class="row"><div class="n-premiacao col-sm-4"><p>3</p></div><div class="premiacao col-sm-8">Prêmios Cobertura Performance</div></div>
      <div class="row"><div class="n-premiacao col-sm-4"><p>3</p></div><div class="premiacao col-sm-8">Certificações Qualidade e Métricas</div></div>
    </div>
    <div class="col-sm-6 col-sm-offset-3" style="text-align: center;">
      <p class="paragrafo">Seguradora americana Top 5 no mundo nos elege como Melhor Projeto nas Américas.</p>
      <button type="button" class="botao botao-home" name="button" data-toggle="modal" data-target=".bd-example-modal-lg4">Mais detalhes</button>
    </div>
  </div>

<!-- Modal -->
<div class="modal-estilo">
  <div id="modal-premiacoes" class="modal fade bd-example-modal-lg4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="">
          <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
        </div>
        <h1>Premiações</h1>
        <hr class="titulo">

        <div class="conteudo-modal">
          Tabela com listagem de premiações<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- Modal final -->

<div class="container">
  <div class="row">

  <div class="col-sm-6 col-left">
    <div class="">
      <p class="titulo1">Missão</p>
      <hr class="hr-titulo1">
      <h1 style="text-align: left;margin-top: 0px;font-size: 40px;">Oferecer soluções de negócios escaláveis, de baixo TCO*,</h1>
      <p style="font-size: 25px;line-height: 25px;color: #15427D;">baseadas em tecnologia, para companhias de Seguros, considerando suas necessidades atuais e futuras</p>
      <p style="font-size: 12px;line-height: 25px;color: #15427D;">*Total Cost of Ownership, uma estimativa financeira de custos diretos e indiretos de investimentos</p>
    </div>
    <div class="">
      <p class="titulo1">Valores</p>
      <hr class="hr-titulo1" style="margin-bottom: 35px;">
      <div class="col-sm-3">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-valores-1.svg">
        <p class="paragrafo-div">Conhecimento em Seguros</p>
      </div>
      <div class="col-sm-3">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-valores-2.svg">
        <p class="paragrafo-div">Flexibilidade</p>
      </div>
      <div class="col-sm-3">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-valores-3.svg">
        <p class="paragrafo-div">Tecnologia</p>
      </div>
      <div class="col-sm-3">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-valores-4.svg">
        <p class="paragrafo-div">Solidez e permanência</p>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-right">
    <div class="row">
      <p class="titulo1">Pilares</p>
      <hr class="hr-titulo1">
    </div>
    <div class="row">
      <div class="col-sm-2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-pilares-1.svg"></div><div class="col-sm-9"><p>Compromisso em ser a melhor relação-custo benefício do mercado.</p></div>
    </div>
    <div class="row">
      <div class="col-sm-2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-pilares-2.svg"></div><div class="col-sm-9"><p>Foco em minimizar o risco de insucesso do projeto.</p></div>
    </div>
    <div class="row">
      <div class="col-sm-2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-pilares-3.svg"></div><div class="col-sm-9"><p>Busca pela eficiência e eficácia em gestão.</p></div>
    </div>
    <div class="row">
      <div class="col-sm-2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-pilares-4.svg"></div><div class="col-sm-9"><p>Capacidade de entender os requisistos do cliente e agregar valor ao seu negócio, por meio dos nossos especialistas.</p></div>
    </div>
    <div class="row">
      <div class="col-sm-2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-pilares-5.svg"></div><div class="col-sm-9"><p>Transparência e ética no relacionamento com clientes, fornecedores, parceiros, colaboradores e acionistas.</p></div>
    </div>
    <div class="row">
      <div class="col-sm-2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-pilares-6.svg"></div><div class="col-sm-9"><p>Aprimoramento continuo de visão de negócio de nossos colaboradores.</p></div>
    </div>
  </div>
  </div>
</div>

<!-- Div destaque -->
<div class="container-fluid div3">
    <div class="row padding-secao">
      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'post',
          'posts_per_page' => 1,
      ));

      // now check if the query has posts and if so, output their content in a banner-box div
      if ( $query->have_posts() ) { ?>

              <?php while ( $query->have_posts() ) : $query->the_post();
              $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-noticias-left">

                  <ul>
                    <?php
                    foreach((get_the_category()) as $category) {
                      echo '<li class="categoria-post">' . $category->cat_name . '</li>';
                    }
                    ?>
                  </ul>
                  <a href="<?php echo esc_url( $url ); ?>"><p class="titulo-post"><?php the_title() ?></p></a>
                  <p class="excerpt-post"><?php echo the_excerpt_max_charlength(100); ?></p><br>
                  <a href="<?php echo get_the_permalink(); ?>"><button type="button" class="botao" name="button">Entenda como</button></a>

                </div>
              <?php endwhile; ?>

      <?php }
      wp_reset_postdata();
      ?>


      <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'post',
            'offset' => 1,
            'posts_per_page' => 1,
        ));

        // now check if the query has posts and if so, output their content in a banner-box div
        if ( $query->have_posts() ) { ?>

              <?php while ( $query->have_posts() ) : $query->the_post();
              $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 col-md-offset-1 col-noticias-right">

                  <a href="<?php echo esc_url( $url ); ?>"><p><?php the_title() ?></p></a>
                  <p><?php echo the_excerpt_max_charlength(155); ?></p><br>
                  <a href="<?php echo get_the_permalink(); ?>"><button type="button" class="botao" name="button">Entenda como</button></a>

                </div>
              <?php endwhile; ?>

        <?php }
        wp_reset_postdata();
        ?>
    </div>
</div>
<!-- fim div3 -->

</div>
<!-- Fim div5 -->

<?php get_footer(); ?>

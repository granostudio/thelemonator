$(document).ready(function() {

// OWL CAROUSEL ===============================================
  $(".owl-carousel-0").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
  $(".grano-carousel-conteudo").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
    $(".grano-carousel-conteudo-1").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,
      nav: true

  });
// /OWL CAROUSEL ==============================================

$(document).ready(function(){
  $(".slider-active").owlCarousel({
  loop:true,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
  });
});


// bxslider crousel ===========================================
  var owlHome = $('.owl-carousel');
  owlHome.owlCarousel({
    items:1,
    dots: false,
    loop:true,
    autoplay:true,
    autoplayHoverPause:true
  });


// Back to Top ================================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ===============================================


// Grano Scroll ANIMATION =====================================
obj = {
  '.textocombotao-1 .row .texto' : 'fadeIn',
  '.textocombotao-2 .row .texto' : 'fadeIn',
  '.textocombotao-3 .row .texto' : 'fadeInUp'
 }

GranoScrollAni(obj);

// / Grano Scroll ANIMATION ===================================

// / Banner home portfolio div3 ===============================


$(document).ready(function () {
        if($(document).width() >= 992){
          $('#Carousel').carousel({
              interval: 5000
          });
      }
});





  
// / Banner home portfolio div3 ===============================


// / saiba mais div4 ==========================================


// / toogle list div1 =========================================
$(document).ready(function () {
  
  $('#toggle-view li').click(function () {

    var text = $(this).children('div.panel');

    if (text.is(':hidden')) {
      text.slideDown('200');
      $(this).children('span').html('-');   
    } else {
      text.slideUp('200');
      $(this).children('span').html('+');   
    }
    
  });

});
// / toogle list div1 =========================================

 


$(document).ready(function () {
        if($(document).width() >= 992){
          (function($) {                            
              $(window).scroll(function(){                          
                  if ($(this).scrollTop() > 250) {
                      $('.navbar').css({"background-color": "rgba(255,255,255,.8)","border-bottom": "1px solid rgba(189, 132, 43, .5)"});
                      $('.navbar-brand img').css({"max-width": "140px","margin-top":"0px"});
                      $('#toggle-menu').css({"top": "1%"});
                  } else {
                      $('.navbar').css({"background": "none","border": "none"});
                      $('.navbar-brand img').css({"max-width": "200px","margin-top":"20px"});
                  }
              });
        })(jQuery);
      }
}); 

                   
// / Toggle Menu ANIMATION ====================================

$("#toggle-menu").click(function() {
  $(this).toggleClass("on");
  $(".navbar-nav").slideToggle();
});

// / Toggle Menu ANIMATION ====================================

                       
  $('.img-seta').on('click', function(){
      $('html, body').animate({
          scrollTop: $('.div2').offset().top -40
      }, 500);
    });
  $('.img-enviar').on('click', function(){
      $('html, body').animate({
          scrollTop: $('.div6').offset().top -40
      }, 500); 
    });  


  $('#menu-item-6').on('click', function(){
    $('html, body').animate({
        scrollTop: $('.div2').offset().top -50
    }, 500);
  }); 
  $('#menu-item-7').on('click', function(){
    $('html, body').animate({
        scrollTop: $('.div3').offset().top -50
    }, 500);
  }); 
  $('#menu-item-8').on('click', function(){
    $('html, body').animate({
        scrollTop: $('.div4').offset().top -50
    }, 500);
  }); 
  $('#menu-item-10').on('click', function(){
    $('html, body').animate({
        scrollTop: $('.div6').offset().top -50
    }, 500);
  });   
  

}); 


       
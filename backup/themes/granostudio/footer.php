<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
?>
<?php wp_footer(); ?>


<?php
// livereload
$host = $_SERVER['HTTP_HOST'];
if ($host == "localhost:9001") {
  ?>
<script src="http://localhost:35729/livereload.js"></script>
  <?php
}

// /livereload
 ?>


<!-- FOOTER ============================================================== -->
<footer>
  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="fa fa-angle-up"></span></a>
  <!-- <p class="float-xs-right"><a href="#">Back to top</a></p> -->
  <!-- <p>&copy; 2014 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p> -->
</footer>
<!-- /FOOTER ============================================================== -->
</div><!-- /.container -->

<!-- Google Analytics -->
<?php $seoanalytics = grano_get_options('seo','seo_analytics');

if(!empty($seoanalytics) || $seoanalytics != "Default Text"){
  echo "<script>";
  echo $seoanalytics;
  echo "</script>";
}

 ?>
<!-- / Google Analytics -->
</div> <!-- / app angular -->
</body>

<!-- Preloader -->
<script type="text/javascript">
    // //<![CDATA[
    //     $(window).on('load', function() { // makes sure the whole site is loaded
    //         $('#status').fadeOut(); // will first fade out the loading animation
    //         $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    //         $('body').delay(350).css({'overflow':'visible'});
    //     })
    // //]]>
</script>

</html>

<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<section class="container">

	<div class="row">
	<div class="col-sm-8 col-sm-offset-2 page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'twentysixteen' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( 'Nada encontrado para sua pesquisa. O que acha de experimentar outras palavras?', 'twentysixteen' ); ?></p>


		<?php else : ?>

			<h1><?php _e( 'Nada encontrado para sua pesquisa, tente  novamente.', 'twentysixteen' ); ?></h1>


		<?php endif; ?>
	</div><!-- .page-content -->
</div>
</section><!-- .no-results -->

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
?>
<?php wp_footer(); ?>


<?php
// livereload
$host = $_SERVER['HTTP_HOST'];
if ($host == "localhost:9001") {
  ?>
<script src="http://localhost:35729/livereload.js"></script>
  <?php
}

// /livereload
 ?>


<!-- FOOTER ============================================================== -->
<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="footer-all">
        <div class="menu-footer">
          <div class="footer-inner">
          <?php
            echo "<div class='col1'>";
              $categories = get_categories();
              echo '<h5>Categorias</h5>';
              echo '<ul>';
              foreach ($categories as $cat) {
                  $category_link = get_category_link( $cat->term_id );
                  $catName = $cat->name;
                  if ($catName != "Sem categoria") {
                      echo '<li ><a href="'.$category_link.'">'.$cat->name.'</a></li>';
                  }
              }
              echo '</ul>';
            echo "</div>";
            echo "<div class='col2'>";
              wp_nav_menu( array(
                  'menu'              => 'footer',
                  'theme_location'    => 'footer',
                  'depth'             => 2,
                  'container'         => 'div',
                  'container_class'   => 'menu',
                  'container_id'      => '',
                  'menu_class'        => 'footer'
                  // 'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                  // 'walker'            => new wp_bootstrap_navwalker())
              ));
            echo "</div>";
            $info_contato = get_option('info_contato');
            $info_tel = $info_contato['cont_telefone'];
            $info_endereco = $info_contato['cont_endereço'];
            $mystring = $info_endereco;
            $endercotag = str_replace("/n", "<br/>", $mystring);


            ?>
            <div class="endereco">
              <?php echo '<h5>Tel.: '.$info_tel.'</h5>'; ?>
              <p>© 2017. GESSAÚDE. Todos os Direitos Reservados<br/>
              <?php echo $endercotag; ?></p>
            </div>
          </div>
        </div>
        <div class="logo">

        </div>
      </div>
    </div>
  </div>
  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Clique para retornar ao topo da página" data-toggle="tooltip" data-placement="left"><span class="fa fa-angle-up"></span></a>

  <?php
  if (is_page( 95 ) ){
    echo '<a id="back-to-topfaq" href="#" class="btn btn-primary btn-lg back-to-topfaq" role="button" title="Clique para retornar as perguntas frequentes" data-toggle="tooltip" data-placement="left"><span class="fa fa-angle-up"></span></a>';
  }
  ?>

  <!-- <p class="float-xs-right"><a href="#">Back to top</a></p> -->
  <!-- <p>&copy; 2014 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p> -->
</footer>
<!-- /FOOTER ============================================================== -->
</div><!-- /.container -->

<!-- Google Analytics -->
<?php $seoanalytics = grano_get_options('seo','seo_analytics');

if(!empty($seoanalytics) || $seoanalytics != "Default Text"){
  echo "<script>";
  echo $seoanalytics;
  echo "</script>";
}

 ?>
<!-- / Google Analytics -->

</body>

<!-- Preloader -->
<script type="text/javascript">
    //<![CDATA[
        $(window).on('load', function() { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(350).css({'overflow':'visible'});
        })
    //]]>
</script>

</html>

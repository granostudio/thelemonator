<!-- conteudo internos variavel -->
<?php
$conteudo = array();
?>

<div class="tituloI">
    <h1>Serviços</h1>
</div>

<div class="row1">

    <div class="coluna-esquerda col1 hidden-xs">
        <div class="fundo"></div>
    </div>

    <div class="coluna2">
      <div class="consultoria">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/livro.png" >
        <h3>CONSULTORIA DE ESTRATÉGIA</h3>
        <a href="#" class="btn btn-primmary leia-mais" data-box=".box-consultoria">Leia Mais</a>
        <!-- <p>Contribuir com a formalização dos objetivos traçados, definição do mapa e modelo de gestão; além do acompanhamento alcance destes objetivos.</p> -->
        <?php
          $conteudo['consultoria'] = '
          <h1>Consultoria de Estrat&eacute;gia</h1>
          <p>A estrat&eacute;gia de uma organiza&ccedil;&atilde;o &eacute; o caminho definido para faz&ecirc;-la alcan&ccedil;ar seus objetivos, a clareza na defini&ccedil;&atilde;o dos objetivos, uma boa estrat&eacute;gia e um excelente processo de acompanhamento s&atilde;o elementos fundamentais para alcan&ccedil;ar o sucesso empresarial.</p>
          <p>A Consultoria de Estrat&eacute;gia da GesSa&uacute;de tem como papel contribuir com a defini&ccedil;&atilde;o/formaliza&ccedil;&atilde;o dos objetivos, definir o mapa e modelo de gest&atilde;o e acompanhamento do alcance destes objetivos.</p>
          <ul>
          <li>Planejamento Estrat&eacute;gico</li>
          <li>Defini&ccedil;&atilde;o/Revis&atilde;o Plano Empresarial</li>
          <li>Defini&ccedil;&atilde;o/Revis&atilde;o do Mapa Estrat&eacute;gico (BSC &ndash; Balanced Scorecard)</li>
          <li>Revis&atilde;o da Arquitetura Organizacional</li>
          <li>Gest&atilde;o de Riscos Corporativos</li>
          <li>Desenvolvimento de modelo de execu&ccedil;&atilde;o estrat&eacute;gica (Or&ccedil;amento, Desempenho, KPIs, Iniciativas</li>
          </ul>
          ';
         ?>

      </div>

      <div class="governanca">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/cabeca.png">
        <h3>GOVERNANÇA CORPORATIVA</h3>
        <a href="#" class="btn btn-primmary leia-mais" data-box=".box-governanca">Leia Mais</a>
        <!-- <p>Otimizar o valor das empresas, a proteção do patrimônio e o equilíbrio dos interesses dos stakeholders de uma organização.</p> -->
        <?php
          $conteudo['governanca'] = '
          <h1>Consultoria de Governan&ccedil;a Corporativa</h1>
          <p>Um bom sistema de governan&ccedil;a corporativa tem como objetivo otimizar o valor das empresas, a prote&ccedil;&atilde;o do patrim&ocirc;nio e o equil&iacute;brio dos interesses dos stakeholders de uma organiza&ccedil;&atilde;o. As boas pr&aacute;ticas de governan&ccedil;a tamb&eacute;m s&atilde;o decisivas para facilitar o acesso &agrave; capital e contribuem para a perenidade do neg&oacute;cio.</p>
          <p>A Consultoria de Governan&ccedil;a da GesSa&uacute;de tem como objetivo preparar um bom sistema de governan&ccedil;a nas institui&ccedil;&otilde;es alinhado a regras de compliance, e que proporcione agilidade para alcan&ccedil;ar seus objetivos estrat&eacute;gicos.</p>
          <ul>
          <li>Diagn&oacute;stico do Modelo de Governan&ccedil;a Corporativa</li>
          <li>Due Diligence de Governan&ccedil;a Corporativa</li>
          <li>Implanta&ccedil;&atilde;o de Conselho de Administra&ccedil;&atilde;o (ou consultivo) e Comit&ecirc;s do Conselho</li>
          <li>Avalia&ccedil;&atilde;o de Desempenho da Alta Gest&atilde;o</li>
          <li>Gerenciamento de Riscos Corporativos</li>
          <li>Compliance e Integridade</li>
          </ul>
          ';
         ?>
      </div>

      <div class="capacitacao">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/servicos-pessoas.png">
        <h3>Programa de Capacitação</h3>
        <a href="#" class="btn btn-primmary leia-mais" data-box=".box-capacitacao">Leia Mais</a>
        <!-- <p>Otimizar o valor das empresas, a proteção do patrimônio e o equilíbrio dos interesses dos stakeholders de uma organização.</p> -->
        <?php
          $conteudo['capacitacao'] = '
          <h1>Programa de Capacita&ccedil;&atilde;o</h1>
<p>Uma boa estrat&eacute;gia e um bom sistema de governan&ccedil;a s&atilde;o complementados por pessoas capacitadas e qualificadas para exercer suas fun&ccedil;&otilde;es t&eacute;cnicas ou de lideran&ccedil;a.</p>
<p>A GesSa&uacute;de desenvolve programas de capacita&ccedil;&atilde;o, sob medida, para aumentar a qualifica&ccedil;&atilde;o e o n&iacute;vel de maturidade gerencial de toda a organiza&ccedil;&atilde;o.</p>
          ';
         ?>
      </div>

    </div>

    <div class="faixa1"></div>
    <div class="faixa01"></div>
</div>

<div class="row2">
    <div class="coluna3">
      <img src="<?php echo get_stylesheet_directory_uri();?>/img/fundo2.png" class="imgresp hidden-xs">
      <div class="gerenciamento">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/calendario.png">
        <h3>Consultoria de Gerenciamento de Processos (BPM)</h3>
        <a href="#" class="btn btn-primmary leia-mais" data-box=".box-gerenciamento">Leia Mais</a>
        <!-- <p>Implantar a cultura de gestão por processos e melhoria contínua; realizar o alinhamento -->
          <!-- entre a estratégia da organização e os processos de negócio; aumentar a eficiência da operação.</p> -->
          <?php
            $conteudo['gerenciamento'] = '
            <h1>Consultoria de Gerenciamento de Processos (BPM)</h1>
            <p>Processos bem definidos e gerenciados podem fazer a diferen&ccedil;a no sucesso de uma organiza&ccedil;&atilde;o, s&atilde;o a base de opera&ccedil;&atilde;o e a sua efici&ecirc;ncia representa poderoso elemento de competitividade e lucratividade.</p>
            <p>A Consultoria em Gerenciamento de Processos tem como objetivos: implantar a cultura de gest&atilde;o por processos e melhoria cont&iacute;nua, realizar o alinhamento entre a estrat&eacute;gia da organiza&ccedil;&atilde;o e os processos de neg&oacute;cio e aumentar a efici&ecirc;ncia da opera&ccedil;&atilde;o.</p>
            <ul>
            <li>Estrat&eacute;gia x Modelo de Processos</li>
            <li>Defini&ccedil;&atilde;o de Governan&ccedil;a de Processos</li>
            <li>Implanta&ccedil;&atilde;o de Arquitetura de Processos</li>
            <li>Implanta&ccedil;&atilde;o do Escrit&oacute;rio de Processos</li>
            <li>Metodologia de Modelagem de Processos</li>
            <li>Otimiza&ccedil;&atilde;o de Processos de Neg&oacute;cio (foco no aumento de desempenho)</li>
            <li>Mapeamento e Modelagem de Processos</li>
            </ul>
            ';
           ?>
      </div>

      <div class="gerenciamentop">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/agenda.png">
        <h3>Gerenciamento de projetos</h3>
        <a href="#" class="btn btn-primmary leia-mais" data-box=".box-gerenciamentop">Leia Mais</a>
        <!-- <p>Implantar a cultura de gerenciamento de projetos; realizar o alinhamento entre a estratégia da organização e os projetos.</p> -->
        <?php
          $conteudo['gerenciamentop'] = '
          <h1>Consultoria de Gerenciamento de Projetos</h1>
          <p>A Consultoria em Gerenciamento de Projetos objetiva: implantar a cultura de gerenciamento de projetos, realizar o alinhamento entre a estrat&eacute;gia da organiza&ccedil;&atilde;o e os projetos, capacitar a organiza&ccedil;&atilde;o para gerenciamento e execu&ccedil;&atilde;o dos projetos.</p>
          <ul>
          <li>Diagn&oacute;stico de Maturidade do ambiente de projetos</li>
          <li>Implanta&ccedil;&atilde;o do Escrit&oacute;rio de Projetos (PMO &ndash; Project Management Office)</li>
          <li>Desenvolvimento de Metodologias (Projeto, Programa, Portf&oacute;lio, Desenvolvimento de Produto, Desenvolvimento de Software)</li>
          <li>Desenvolvimento de Plano de Compet&ecirc;ncias de Gerenciamento de Projetos</li>
          </ul>
          ';
         ?>
      </div>
    </div>

    <div class="coluna-direita col2  hidden-xs">
      <div class="fundo"></div>
    </div>

    <div class="faixa2"></div>
    <div class="faixa02"></div>

</div>

<div class="row1 row-final">
    <div class="coluna-esquerda col3 hidden-xs">
      <div class="fundo"></div>
    </div>

    <div class="coluna6">
      <div class="governanca">
        <h3>GOVERNANÇA DE TI</h3>
        <a href="#" class="btn btn-primmary leia-mais" data-box=".box-governancati">Leia Mais</a>
        <!-- <p>Garantir que as entregas e serviços da TI estejam alinhados à estratégia da organização;proporcionar ganhos efetivos de melhoria e eficiência no negócio, que gerem aumento de competitividade.</p> -->
        <?php
          $conteudo['governancati'] = '
          <h1>Consultoria de Governan&ccedil;a de TI</h1>
          <p>A Consultoria em Governan&ccedil;a de TI da GesSa&uacute;de visa: garantir que as entregas e servi&ccedil;os da TI estejam alinhados a estrat&eacute;gia da organiza&ccedil;&atilde;o, proporcionar ganhos efetivos de melhoria e efici&ecirc;ncia no neg&oacute;cio que gerem aumento de competitividade.</p>
          <ul>
          <li>Diagn&oacute;stico de Maturidade de TI</li>
          <li>Plano Estrat&eacute;gico de TI</li>
          <li>Defini&ccedil;&atilde;o de Processos de TI</li>
          <li>Sele&ccedil;&atilde;o de Sistemas de Gest&atilde;o (ERP) &ndash; Software Selection</li>
          <li>Avalia&ccedil;&atilde;o de Ader&ecirc;ncia do ERP</li>
          <li>Suporte Adequa&ccedil;&atilde;o com Fornecedores Atuais</li>
          <li>Suporte na Sele&ccedil;&atilde;o de Novos Fornecedores</li>
          <li>Modelagem de Processos de Neg&oacute;cio para implanta&ccedil;&atilde;o de ERP</li>
          <li>Gerenciamento de Projeto de Implanta&ccedil;&atilde;o de ERP</li>
          </ul>
          ';
         ?>
      </div>
    </div>

    <div class="faixa3"></div>
</div>


<!-- box conteudo loop -->

<?php foreach ($conteudo as $key => $value) {
  ?>
  <div class="box-conteudo box-<?php echo $key ?>">
    <div class="btnFechar"></div>
    <?php echo $value; ?>
  </div>
  <?php
} ?>


<div class="row4">
  <div class="mask"></div>
  <div class="proama">
      <h3>PROGRAMA DE CAPACITAÇÃO E ACELERAÇÃO DE MATURIDADE DA GESTÃO DA SAÚDE (PROAMA)</h3>
      <p>Reúne instituições de Saúde (hospitais, clínicas, centros de diagnósticos, etc.) de pequeno e médio portes interessados em elevar seus resultados e aumentar a maturidade de gestão hospitalar</p>
      <a class="btn btn-primary" href="/proama/" style="color:white">Saiba Mais</a>
  </div>
  <canvas></canvas>
</div>

<script src="<?php echo get_stylesheet_directory_uri() . '/js/network-ani.js' ?>" charset="utf-8"></script>

<script type="text/javascript">


$(document).ready(function() {

// box-conteudo ====================================

$('.leia-mais').on('click', function(event) {
  event.preventDefault();
  /* Act on the event */
  var box = $(this).data('box');
  $(box).addClass('active');
});
$('.box-conteudo .btnFechar').on('click', function(event) {
  event.preventDefault();
  /* Act on the event */
  $('.box-conteudo').removeClass('active');
});
$('.box-conteudo .btn').on('click', function(event) {
  // event.preventDefault();
  /* Act on the event */
  $('.box-conteudo').removeClass('active');
});
$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
      $('.box-conteudo').removeClass('active');
    } else{

    }
});


});
</script>
